<?php

namespace App\DataProvider;

use ApiPlatform\Core\Bridge\Doctrine\Orm\Extension\PaginationExtension;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Extension\QueryResultCollectionExtensionInterface;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Util\QueryNameGenerator;
use ApiPlatform\Core\DataProvider\ContextAwareCollectionDataProviderInterface;
use ApiPlatform\Core\DataProvider\RestrictedDataProviderInterface;
use App\Entity\Judge;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class JudgeDataProvider implements ContextAwareCollectionDataProviderInterface, RestrictedDataProviderInterface
{
    public function __construct(
        private readonly EntityManagerInterface $em,
        private readonly PaginationExtension $pagination,
        private readonly TokenStorageInterface $tokenStorage,
    ) {
    }

    public function supports(string $resourceClass, string $operationName = null, array $context = []): bool
    {
        return $resourceClass === Judge::class;
    }

    public function getCollection(string $resourceClass, string $operationName = null, array $context = []): iterable
    {
        $queryBuilder = $this->em->getRepository($resourceClass)
            ->createQueryBuilder("entity")
            ->andWhere("entity.id = :id")
            ->setParameter('id', $this->tokenStorage->getToken()->getUser()->getId());

        $this->pagination->applyToCollection($queryBuilder, new QueryNameGenerator(), $resourceClass, $operationName,
            $context);

        if ($this->pagination instanceof QueryResultCollectionExtensionInterface && $this->pagination->supportsResult($resourceClass,
                $operationName, $context)) {
            return $this->pagination->getResult($queryBuilder, $resourceClass, $operationName, $context);
        }

        return $queryBuilder->getQuery()->getResult();
    }
}