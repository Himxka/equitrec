<?php

namespace App\EventSubscriber;

use App\Entity\Competition;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use EasyCorp\Bundle\EasyAdminBundle\Event\BeforeEntityPersistedEvent;
use Symfony\Bridge\Twig\Mime\NotificationEmail;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Twig\Environment;


class CompetitionSubscriber implements EventSubscriberInterface
{
    public function __construct(
        private readonly UserPasswordHasherInterface $hasher,
        private readonly MailerInterface $mailer,
        private readonly EntityManagerInterface $em,
        private readonly Environment $twig,
    ) {
    }

    public static function getSubscribedEvents(): array
    {
        return [
            BeforeEntityPersistedEvent::class => ['computeData'],
        ];
    }

    public function computeData(BeforeEntityPersistedEvent $args): void
    {
        $entity = $args->getEntityInstance();

        if (!$entity instanceof Competition) {
            return;
        }

        $user = $this->em->getRepository(User::class)->findOneBy(['email' => $entity->getAdmin()->getEmail()]);

        if ($user) {
            $entity->setAdmin($user);
            $password = "Votre mot de passe habituel";
        } else {
            $password = $this->generatePassword();
            $entity->getAdmin()->setPassword($this->hasher->hashPassword($entity->getAdmin(), $password));
            $entity->getAdmin()->setRoles(['ROLE_LOCAL_ADMIN']);
        }

        $email = new NotificationEmail();
        $email->markAsPublic();

        $email
            ->to($entity->getAdmin()->getEmail())
            ->subject('[EQUITREC] Nouvelle compétition');

        $email->htmlTemplate("emails/competition.html.twig");
        $email->context(['user' => $entity->getAdmin(), 'password' => $password]);


        $this->mailer->send($email);

    }

    private function generatePassword($length = 12)
    {
        $chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz' .
            '0123456789`-=~!@#$%^&*()_+,./<>?;:[]{}\|';

        $str = '';
        $max = strlen($chars) - 1;

        for ($i = 0; $i < $length; $i++) {
            $str .= $chars[random_int(0, $max)];
        }

        return $str;
    }
}
