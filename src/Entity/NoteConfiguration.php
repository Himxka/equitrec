<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\NoteConfigurationRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: NoteConfigurationRepository::class)]
#[ApiResource(
    collectionOperations: [],
    itemOperations: ["GET"],
    denormalizationContext: ['groups' => []],
    normalizationContext: ['groups' => ['configuration:read']],
)]
class NoteConfiguration
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    #[Groups(['configuration:read'])]
    private ?int $id;

    #[ORM\Column(type: 'integer', nullable: true)]
    #[Groups(['configuration:read'])]
    private ?int $value = 0;

    #[ORM\Column(type: 'string', length: 255)]
    #[Groups(['configuration:read'])]
    private ?string $name = "";

    #[ORM\ManyToOne(targetEntity: ObstacleConfiguration::class, inversedBy: 'contractNotes')]
    #[ORM\JoinColumn(nullable: true)]
    private ?ObstacleConfiguration $obstacleContractConfiguration;

    #[ORM\ManyToOne(targetEntity: ObstacleConfiguration::class, inversedBy: 'styleNotes')]
    #[ORM\JoinColumn(nullable: true)]
    private ?ObstacleConfiguration $obstacleStyleConfiguration;

    #[ORM\ManyToOne(targetEntity: ObstacleConfiguration::class, inversedBy: 'paceNotes')]
    #[ORM\JoinColumn(nullable: true)]
    private ?ObstacleConfiguration $obstaclePaceConfiguration;

    #[ORM\ManyToOne(targetEntity: ObstacleConfiguration::class, inversedBy: 'penaltyNotes')]
    #[ORM\JoinColumn(nullable: true)]
    private ?ObstacleConfiguration $obstaclePenaltyConfiguration;

    public function __toString(): string
    {
        return "$this->name - $this->value";
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getMin(): ?int
    {
        return $this->min;
    }

    public function setMin(int $min): self
    {
        $this->min = $min;

        return $this;
    }

    public function getMax(): ?int
    {
        return $this->max;
    }

    public function setMax(int $max): self
    {
        $this->max = $max;

        return $this;
    }

    public function getValue(): ?int
    {
        return $this->value;
    }

    public function setValue(int $value): self
    {
        $this->value = $value;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return ObstacleConfiguration|null
     */
    public function getObstacleStyleConfiguration(): ?ObstacleConfiguration
    {
        return $this->obstacleStyleConfiguration;
    }

    /**
     * @param ObstacleConfiguration|null $obstacleStyleConfiguration
     * @return NoteConfiguration
     */
    public function setObstacleStyleConfiguration(?ObstacleConfiguration $obstacleStyleConfiguration): NoteConfiguration
    {
        $this->obstacleStyleConfiguration = $obstacleStyleConfiguration;
        return $this;
    }

    /**
     * @return ObstacleConfiguration|null
     */
    public function getObstaclePenaltyConfiguration(): ?ObstacleConfiguration
    {
        return $this->obstaclePenaltyConfiguration;
    }

    /**
     * @param ObstacleConfiguration|null $obstaclePenaltyConfiguration
     * @return NoteConfiguration
     */
    public function setObstaclePenaltyConfiguration(?ObstacleConfiguration $obstaclePenaltyConfiguration
    ): NoteConfiguration {
        $this->obstaclePenaltyConfiguration = $obstaclePenaltyConfiguration;
        return $this;
    }

    /**
     * @return ObstacleConfiguration|null
     */
    public function getObstacleContractConfiguration(): ?ObstacleConfiguration
    {
        return $this->obstacleContractConfiguration;
    }

    /**
     * @param ObstacleConfiguration|null $obstacleContractConfiguration
     * @return NoteConfiguration
     */
    public function setObstacleContractConfiguration(?ObstacleConfiguration $obstacleContractConfiguration
    ): NoteConfiguration {
        $this->obstacleContractConfiguration = $obstacleContractConfiguration;
        return $this;
    }

    /**
     * @return ObstacleConfiguration|null
     */
    public function getObstaclePaceConfiguration(): ?ObstacleConfiguration
    {
        return $this->obstaclePaceConfiguration;
    }

    /**
     * @param ObstacleConfiguration|null $obstaclePaceConfiguration
     * @return NoteConfiguration
     */
    public function setObstaclePaceConfiguration(?ObstacleConfiguration $obstaclePaceConfiguration): NoteConfiguration
    {
        $this->obstaclePaceConfiguration = $obstaclePaceConfiguration;
        return $this;
    }
}
