<?php

namespace App\Entity;

use App\Repository\PenaltyConfigurationRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: PenaltyConfigurationRepository::class)]
class PenaltyConfiguration
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    #[Groups(['configuration:read'])]
    private ?int $id;

    #[ORM\Column(type: 'string', length: 255)]
    #[Groups(['configuration:read'])]
    private ?string $name;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    #[Groups(['configuration:read'])]
    private ?string $description;

    #[ORM\ManyToOne(targetEntity: ObstacleConfiguration::class, inversedBy: 'penaltyConfigurations')]
    #[ORM\JoinColumn(nullable: false)]
    private ObstacleConfiguration $obstacleConfiguration;

    public function __toString(): string
    {
        return $this->name;
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int|null $id
     * @return PenaltyConfiguration
     */
    public function setId(?int $id): PenaltyConfiguration
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string|null $name
     * @return PenaltyConfiguration
     */
    public function setName(?string $name): PenaltyConfiguration
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @param string|null $description
     * @return PenaltyConfiguration
     */
    public function setDescription(?string $description): PenaltyConfiguration
    {
        $this->description = $description;
        return $this;
    }

    public function getObstacleConfiguration(): ?ObstacleConfiguration
    {
        return $this->obstacleConfiguration;
    }

    public function setObstacleConfiguration(?ObstacleConfiguration $obstacleConfiguration): self
    {
        $this->obstacleConfiguration = $obstacleConfiguration;

        return $this;
    }
}
