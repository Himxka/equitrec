<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\JudgeRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\IdGenerator\UuidGenerator;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Uid\Uuid;

#[ORM\Entity(repositoryClass: JudgeRepository::class)]
#[ORM\UniqueConstraint(name: "unique_judge", columns: ['first_name', 'last_name', 'competition_id'])]
#[UniqueEntity(fields: ['firstName', 'lastName', 'competition'])]
#[ApiResource(
    collectionOperations: ["GET"],
    itemOperations: ["GET"],
    denormalizationContext: ['groups' => ['judge:write']],
    normalizationContext: ['groups' => ['judge:read', 'configuration:read']],
)]
class Judge implements UserInterface
{
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'CUSTOM')]
    #[ORM\CustomIdGenerator(class: UuidGenerator::class)]
    #[ORM\Column(type: 'uuid')]
    #[Groups(['judge:read'])]
    private Uuid $id;

    #[ORM\Column(type: 'string', length: 255)]
    private ?string $token;

    #[ORM\Column(type: 'boolean')]
    private bool $tokenUsed = false;

    #[ORM\Column(type: 'string', length: 255)]
    #[Groups(['judge:read'])]
    private ?string $firstName;

    #[ORM\Column(type: 'string', length: 255)]
    #[Groups(['judge:read'])]
    private ?string $lastName;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private ?string $phoneIdentifier;

    #[ORM\ManyToOne(targetEntity: Competition::class, inversedBy: 'judges')]
    #[ORM\JoinColumn(nullable: false)]
    #[Groups(['judge:read'])]
    private ?Competition $competition;

    #[ORM\ManyToMany(targetEntity: ObstacleConfiguration::class)]
    #[Groups(['judge:read'])]
    private Collection $obstacles;

    public function __toString(): string
    {
        return "$this->firstName $this->lastName";
    }

    public function __construct()
    {
        $this->token = bin2hex(random_bytes(50));
        $this->obstacles = new ArrayCollection();
    }

    public function getId(): Uuid
    {
        return $this->id;
    }

    public function getToken(): ?string
    {
        return $this->token;
    }

    public function setToken(string $token): self
    {
        $this->token = $token;

        return $this;
    }

    public function isTokenUsed(): ?bool
    {
        return $this->tokenUsed;
    }

    public function setTokenUsed(bool $tokenUsed): self
    {
        $this->tokenUsed = $tokenUsed;

        return $this;
    }

    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    public function setFirstName(string $firstName): self
    {
        $this->firstName = $firstName;

        return $this;
    }

    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    public function setLastName(string $lastName): self
    {
        $this->lastName = $lastName;

        return $this;
    }

    public function getPhoneIdentifier(): ?string
    {
        return $this->phoneIdentifier;
    }

    public function setPhoneIdentifier(string $phoneIdentifier): self
    {
        $this->phoneIdentifier = $phoneIdentifier;

        return $this;
    }

    public function getCompetition(): ?Competition
    {
        return $this->competition;
    }

    public function setCompetition(?Competition $competition): self
    {
        $this->competition = $competition;

        return $this;
    }

    public function getRoles(): array
    {
        return ['ROLE_JUDGE'];
    }

    public function getPassword(): string
    {
        return $this->token;
    }

    public function getSalt(): void
    {
    }

    public function eraseCredentials()
    {
        // TODO: Implement eraseCredentials() method.
    }

    public function getUsername(): string
    {
        return $this->getUserIdentifier();
    }

    public function getUserIdentifier(): string
    {
        return $this->token;
    }

    /**
     * @return Collection<int, ObstacleConfiguration>
     */
    public function getObstacles(): Collection
    {
        return $this->obstacles;
    }

    public function addObstacle(ObstacleConfiguration $obstacle): self
    {
        if (!$this->obstacles->contains($obstacle)) {
            $this->obstacles[] = $obstacle;
        }

        return $this;
    }

    public function removeObstacle(ObstacleConfiguration $obstacle): self
    {
        $this->obstacles->removeElement($obstacle);

        return $this;
    }
}
