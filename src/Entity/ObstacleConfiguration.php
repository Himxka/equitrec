<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\ObstacleConfigurationRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: ObstacleConfigurationRepository::class)]
#[ApiResource(
    collectionOperations: [],
    itemOperations: ["GET"],
    denormalizationContext: ['groups' => []],
    normalizationContext: ['groups' => ['configuration:read']],
)]
class ObstacleConfiguration
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    #[Groups(['configuration:read'])]
    private ?int $id;

    #[ORM\Column(type: 'string', length: 255)]
    #[Groups(['configuration:read'])]
    private ?string $name;

    #[ORM\Column(type: 'text', nullable: true)]
    #[Groups(['configuration:read'])]
    private ?string $description;

    #[ORM\Column(type: 'text', nullable: true)]
    #[Groups(['configuration:read'])]
    private string $materials;

    #[ORM\Column(type: 'text', nullable: true)]
    #[Groups(['configuration:read'])]
    private string $characteristics;

    #[ORM\ManyToOne(targetEntity: TrialType::class)]
    #[ORM\JoinColumn(nullable: false)]
    #[Groups(['configuration:read'])]
    private ?TrialType $trialType;

    #[ORM\OneToMany(mappedBy: 'obstacleContractConfiguration', targetEntity: NoteConfiguration::class, cascade: ['persist'])]
    #[Groups(['configuration:read'])]
    private Collection $contractNotes;

    #[ORM\OneToMany(mappedBy: 'obstacleStyleConfiguration', targetEntity: NoteConfiguration::class, cascade: ['persist'])]
    #[Groups(['configuration:read'])]
    private Collection $styleNotes;

    #[ORM\OneToMany(mappedBy: 'obstaclePenaltyConfiguration', targetEntity: NoteConfiguration::class, cascade: ['persist'])]
    #[Groups(['configuration:read'])]
    private Collection $penaltyNotes;

    #[ORM\OneToMany(mappedBy: 'obstaclePaceConfiguration', targetEntity: NoteConfiguration::class, cascade: ['persist'])]
    #[Groups(['configuration:read'])]
    private Collection $paceNotes;

    #[ORM\OneToMany(mappedBy: 'obstacleConfiguration', targetEntity: PenaltyConfiguration::class, cascade: ['persist'], orphanRemoval: true)]
    #[Groups(['configuration:read'])]
    private Collection $penaltyConfigurations;

    public function __construct()
    {
        $this->styleNotes = new ArrayCollection();
        $this->penaltyNotes = new ArrayCollection();
        $this->contractNotes = new ArrayCollection();
        $this->paceNotes = new ArrayCollection();
        $this->penaltyConfigurations = new ArrayCollection();
    }

    public function __toString(): string
    {
        return $this->name;
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int|null $id
     * @return ObstacleConfiguration
     */
    public function setId(?int $id): ObstacleConfiguration
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string|null $name
     * @return ObstacleConfiguration
     */
    public function setName(?string $name): ObstacleConfiguration
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @param string|null $description
     * @return ObstacleConfiguration
     */
    public function setDescription(?string $description): ObstacleConfiguration
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @return string
     */
    public function getMaterials(): string
    {
        return $this->materials;
    }

    /**
     * @param string $materials
     * @return ObstacleConfiguration
     */
    public function setMaterials(string $materials): ObstacleConfiguration
    {
        $this->materials = $materials;
        return $this;
    }

    /**
     * @return string
     */
    public function getCharacteristics(): string
    {
        return $this->characteristics;
    }

    /**
     * @param string $characteristics
     * @return ObstacleConfiguration
     */
    public function setCharacteristics(string $characteristics): ObstacleConfiguration
    {
        $this->characteristics = $characteristics;
        return $this;
    }

    public function getTrialType(): ?TrialType
    {
        return $this->trialType;
    }

    public function setTrialType(?TrialType $trialType): self
    {
        $this->trialType = $trialType;

        return $this;
    }

    /**
     * @return Collection<int, NoteConfiguration>
     */
    public function getStyleNotes(): Collection
    {
        return $this->styleNotes;
    }

    public function addStyleNote(NoteConfiguration $styleNote): self
    {
        if (!$this->styleNotes->contains($styleNote)) {
            $this->styleNotes[] = $styleNote;
            $styleNote->setObstacleStyleConfiguration($this);
        }

        return $this;
    }

    public function removeStyleNote(NoteConfiguration $styleNote): self
    {
        if ($this->styleNotes->removeElement($styleNote)) {
            // set the owning side to null (unless already changed)
            if ($styleNote->getObstacleStyleConfiguration() === $this) {
                $styleNote->setObstacleStyleConfiguration(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, NoteConfiguration>
     */
    public function getPenaltyNotes(): Collection
    {
        return $this->penaltyNotes;
    }

    public function addPenaltyNote(NoteConfiguration $penaltyNote): self
    {
        if (!$this->penaltyNotes->contains($penaltyNote)) {
            $this->penaltyNotes[] = $penaltyNote;
            $penaltyNote->setObstaclePenaltyConfiguration($this);
        }

        return $this;
    }

    public function removePenaltyNote(NoteConfiguration $penaltyNote): self
    {
        if ($this->penaltyNotes->removeElement($penaltyNote)) {
            // set the owning side to null (unless already changed)
            if ($penaltyNote->getObstaclePenaltyConfiguration() === $this) {
                $penaltyNote->setObstaclePenaltyConfiguration(null);
            }
        }

        return $this;
    }

    public function getContractNotes(): Collection
    {
        return $this->contractNotes;
    }

    public function addContractNote(NoteConfiguration $penaltyNote): self
    {
        if (!$this->contractNotes->contains($penaltyNote)) {
            $this->contractNotes[] = $penaltyNote;
            $penaltyNote->setObstacleContractConfiguration($this);
        }

        return $this;
    }

    public function removeContractNote(NoteConfiguration $penaltyNote): self
    {
        if ($this->contractNotes->removeElement($penaltyNote)) {
            // set the owning side to null (unless already changed)
            if ($penaltyNote->getObstacleContractConfiguration() === $this) {
                $penaltyNote->setObstacleContractConfiguration(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, PenaltyConfiguration>
     */
    public function getPenaltyConfigurations(): Collection
    {
        return $this->penaltyConfigurations;
    }

    public function addPenaltyConfiguration(PenaltyConfiguration $penaltyConfiguration): self
    {
        if (!$this->penaltyConfigurations->contains($penaltyConfiguration)) {
            $this->penaltyConfigurations[] = $penaltyConfiguration;
            $penaltyConfiguration->setObstacleConfiguration($this);
        }

        return $this;
    }

    public function removePenaltyConfiguration(PenaltyConfiguration $penaltyConfiguration): self
    {
        if ($this->penaltyConfigurations->removeElement($penaltyConfiguration)) {
            // set the owning side to null (unless already changed)
            if ($penaltyConfiguration->getObstacleConfiguration() === $this) {
                $penaltyConfiguration->setObstacleConfiguration(null);
            }
        }

        return $this;
    }

    public function getPaceNotes(): Collection
    {
        return $this->paceNotes;
    }

    public function addPaceNote(NoteConfiguration $penaltyNote): self
    {
        if (!$this->paceNotes->contains($penaltyNote)) {
            $this->paceNotes[] = $penaltyNote;
            $penaltyNote->setObstaclePaceConfiguration($this);
        }

        return $this;
    }

    public function removePaceNote(NoteConfiguration $penaltyNote): self
    {
        if ($this->paceNotes->removeElement($penaltyNote)) {
            // set the owning side to null (unless already changed)
            if ($penaltyNote->getObstaclePaceConfiguration() === $this) {
                $penaltyNote->setObstaclePaceConfiguration(null);
            }
        }

        return $this;
    }
}
