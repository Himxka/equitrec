<?php

namespace App\Entity;

use App\Repository\AddressRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: AddressRepository::class)]
class Address
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    #[Groups(['competition:read'])]
    private ?int $id;

    #[ORM\Column(type: 'string', length: 255)]
    #[Groups(['competition:read'])]
    private ?string $address;

    #[ORM\Column(type: 'string', length: 255)]
    #[Groups(['competition:read'])]
    private ?string $zip;

    #[ORM\Column(type: 'string', length: 255)]
    #[Groups(['competition:read'])]
    private ?string $city;

    #[ORM\OneToOne(mappedBy: 'address', targetEntity: Competition::class, cascade: ['persist', 'remove'])]
    private Competition $competition;

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getAddress(): ?string
    {
        return $this->address;
    }

    /**
     * @param string|null $address
     */
    public function setAddress(?string $address): void
    {
        $this->address = $address;
    }

    /**
     * @return string|null
     */
    public function getZip(): ?string
    {
        return $this->zip;
    }

    /**
     * @param string|null $zip
     */
    public function setZip(?string $zip): void
    {
        $this->zip = $zip;
    }

    /**
     * @return string|null
     */
    public function getCity(): ?string
    {
        return $this->city;
    }

    /**
     * @param string|null $city
     */
    public function setCity(?string $city): void
    {
        $this->city = $city;
    }

    public function getCompetition(): ?Competition
    {
        return $this->competition;
    }

    public function setCompetition(Competition $competition): self
    {
        // set the owning side of the relation if necessary
        if ($competition->getAddress() !== $this) {
            $competition->setAddress($this);
        }

        $this->competition = $competition;

        return $this;
    }
}
