<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\CompetitionRepository;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: CompetitionRepository::class)]
#[ORM\UniqueConstraint(name: "unique_competition_token", columns: ['token'])]
#[UniqueEntity(fields: ["token"])]
#[ApiResource(
    collectionOperations: ["GET"],
    itemOperations: ["GET"],
    denormalizationContext: ['groups' => ['competition:write']],
    normalizationContext: ['groups' => ['competition:read', 'configuration:read', 'rider:read']],
)]
class Competition
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    #[Groups(['competition:read'])]
    private ?int $id;

    #[ORM\Column(type: 'datetime')]
    #[Groups(['competition:read'])]
    private DateTime $startDate;

    #[ORM\Column(type: 'datetime')]
    #[Groups(['competition:read'])]
    private DateTime $endDate;

    #[ORM\Column(type: 'string', length: 255)]
    private string $token;

    #[ORM\OneToOne(inversedBy: 'competition', targetEntity: Address::class, cascade: ['persist', 'remove'])]
    #[ORM\JoinColumn(nullable: false)]
    #[Groups(['competition:read'])]
    private ?Address $address;

    #[ORM\OneToMany(mappedBy: 'competition', targetEntity: Judge::class, orphanRemoval: true)]
    private Collection $judges;

    #[ORM\Column(type: 'string', length: 255)]
    #[Groups(['competition:read'])]
    private ?string $title;

    #[ORM\ManyToOne(targetEntity: User::class, cascade: ['persist'])]
    #[ORM\JoinColumn(nullable: false)]
    private ?User $admin;

    #[ORM\OneToMany(mappedBy: 'competition', targetEntity: Rider::class, orphanRemoval: true)]
    #[Groups(['competition:read'])]
    private Collection $riders;

    public function __toString(): string
    {
        return $this->getTitle();
    }

    public function __construct()
    {
        $this->startDate = new DateTime();
        $this->endDate = new DateTime();
        $this->token = bin2hex(random_bytes(50));
        $this->judges = new ArrayCollection();
        $this->riders = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getStartDate(): ?\DateTime
    {
        return $this->startDate;
    }

    public function setStartDate(DateTime $startDate): self
    {
        $this->startDate = $startDate;

        return $this;
    }

    public function getEndDate(): ?DateTime
    {
        return $this->endDate;
    }

    public function setEndDate(DateTime $endDate): self
    {
        $this->endDate = $endDate;

        return $this;
    }

    public function getToken(): ?string
    {
        return $this->token;
    }

    public function setToken(string $token): self
    {
        $this->token = $token;

        return $this;
    }

    public function getAddress(): ?Address
    {
        return $this->address;
    }

    public function setAddress(Address $address): self
    {
        $this->address = $address;

        return $this;
    }

    /**
     * @return Collection<int, Judge>
     */
    public function getJudges(): Collection
    {
        return $this->judges;
    }

    public function addJudge(Judge $judge): self
    {
        if (!$this->judges->contains($judge)) {
            $this->judges[] = $judge;
            $judge->setCompetition($this);
        }

        return $this;
    }

    public function removeJudge(Judge $judge): self
    {
        // set the owning side to null (unless already changed)
        if ($this->judges->removeElement($judge) && $judge->getCompetition() === $this) {
            $judge->setCompetition(null);
        }

        return $this;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getAdmin(): ?User
    {
        return $this->admin;
    }

    public function setAdmin(User $admin): self
    {
        $this->admin = $admin;

        return $this;
    }

    /**
     * @return Collection<int, Rider>
     */
    public function getRiders(): Collection
    {
        return $this->riders;
    }

    public function addRider(Rider $rider): self
    {
        if (!$this->riders->contains($rider)) {
            $this->riders[] = $rider;
            $rider->setCompetition($this);
        }

        return $this;
    }

    public function removeRider(Rider $rider): self
    {
        if ($this->riders->removeElement($rider)) {
            // set the owning side to null (unless already changed)
            if ($rider->getCompetition() === $this) {
                $rider->setCompetition(null);
            }
        }

        return $this;
    }
}
