<?php

namespace App\Entity;

use App\Repository\TrialTypeRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: TrialTypeRepository::class)]
class TrialType
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    #[Groups(['configuration:read'])]
    private ?int $id;

    #[ORM\Column(type: 'string', length: 255)]
    #[Groups(['configuration:read'])]
    private ?string $name;

    public function __toString(): string
    {
        return $this->getName();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }
}
