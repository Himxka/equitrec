<?php

namespace App\DataPersister;

use ApiPlatform\Core\DataPersister\ContextAwareDataPersisterInterface;
use App\Entity\Judge;
use App\Entity\ObstacleNote;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class ObstacleNoteDataPersister implements ContextAwareDataPersisterInterface
{
    public function __construct(
        private EntityManagerInterface $em,
        private TokenStorageInterface $tokenStorage,
    ) {
    }

    public function supports($data, array $context = []): bool
    {
        return $data instanceof ObstacleNote;
    }

    /**
     * @param ObstacleNote $data
     * @param array $context
     * @return ObstacleNote
     */
    public function persist($data, array $context = []): ObstacleNote
    {
        $judge = $this->em->getRepository(Judge::class)->find($this->tokenStorage->getToken()->getUser()->getId());
        $data->setJudge($judge);
        $this->em->persist($data);
        $this->em->flush();

        return $data;
    }

    /**
     * @param User $data
     * @param array $context
     * @return void
     */
    public function remove($data, array $context = []): void
    {
        $this->em->remove($data);
        $this->em->flush();
    }
}