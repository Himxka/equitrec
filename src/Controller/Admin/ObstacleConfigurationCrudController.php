<?php

namespace App\Controller\Admin;

use App\Entity\ObstacleConfiguration;
use App\Form\NoteType;
use App\Form\PenaltyType;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\CollectionField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

#[IsGranted('ROLE_SUPER_ADMIN')]
class ObstacleConfigurationCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return ObstacleConfiguration::class;
    }

    public function configureActions(Actions $actions): Actions
    {
        return $actions
            ->add(Crud::PAGE_INDEX, Action::DETAIL);
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id')->hideOnForm(),
            TextField::new('name'),
            AssociationField::new('trialType')->setCrudController(TrialTypeCrudController::class),
            TextEditorField::new('description')->hideOnIndex()->setTemplatePath("/admin/field/textEditor.html.twig"),
            TextEditorField::new('materials')->hideOnIndex()->setTemplatePath("/admin/field/textEditor.html.twig"),
            TextEditorField::new('characteristics')->hideOnIndex()->setTemplatePath("/admin/field/textEditor.html.twig"),

            CollectionField::new('contractNotes')->setEntryType(NoteType::class)->setTemplatePath("/admin/field/noteField.html.twig")->hideOnIndex(),
            CollectionField::new('styleNotes')->setEntryType(NoteType::class)->setTemplatePath("/admin/field/noteField.html.twig")->hideOnIndex(),
            CollectionField::new('paceNotes')->setEntryType(NoteType::class)->setTemplatePath("/admin/field/noteField.html.twig")->hideOnIndex(),
            CollectionField::new('penaltyNotes')->setEntryType(NoteType::class)->setTemplatePath("/admin/field/noteField.html.twig")->hideOnIndex(),

            CollectionField::new('penaltyConfigurations')->setEntryType(PenaltyType::class)->hideOnIndex(),
        ];
    }
}
