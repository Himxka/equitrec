<?php

namespace App\Controller\Admin;

use App\Entity\Competition;
use App\Field\AddressField;
use App\Field\AdminField;
use Doctrine\ORM\QueryBuilder;
use EasyCorp\Bundle\EasyAdminBundle\Collection\FieldCollection;
use EasyCorp\Bundle\EasyAdminBundle\Collection\FilterCollection;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Dto\EntityDto;
use EasyCorp\Bundle\EasyAdminBundle\Dto\SearchDto;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Orm\EntityRepository;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class CompetitionCrudController extends AbstractCrudController
{

    public function __construct(
        private readonly EntityRepository $entityRepository,
        private readonly UrlGeneratorInterface $urlGenerator
    ) {
    }

    public static function getEntityFqcn(): string
    {
        return Competition::class;
    }

    public function configureActions(Actions $actions): Actions
    {

        $impersonate = Action::new('impersonate', 'Impersonate')
            ->linkToUrl(function (Competition $competition): string {
                return $this->urlGenerator->generate(
                    'app_admin_dashboard_index',
                    ['_switch_user' => $competition->getAdmin()?->getUserIdentifier()],
                    UrlGeneratorInterface::ABSOLUTE_URL
                );
            });

        if ($_ENV['APP_ENV'] === "dev") {
            $actions->add(Crud::PAGE_INDEX, $impersonate);
        }

        $actions->setPermissions([
            'list' => 'ROLE_ADMIN',
            'show' => 'ROLE_ADMIN',
            'edit' => 'ROLE_ADMIN',
            'delete' => 'ROLE_SUPER_ADMIN',
            'new' => 'ROLE_SUPER_ADMIN',
        ]);
        return parent::configureActions($actions);
    }

    public function createIndexQueryBuilder(
        SearchDto $searchDto,
        EntityDto $entityDto,
        FieldCollection $fields,
        FilterCollection $filters
    ): QueryBuilder {
        parent::createIndexQueryBuilder($searchDto, $entityDto, $fields, $filters);

        $response = $this->entityRepository->createQueryBuilder($searchDto, $entityDto, $fields, $filters);

        if (!$this->isGranted("ROLE_SUPER_ADMIN")) {
            $response->andWhere("entity.admin = :user")->setParameter("user", $this->getUser());
        }

        return $response;
    }


    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id')->hideOnForm(),
            TextField::new('title'),
            DateTimeField::new('startDate')->setTimezone("Europe/Paris")->setFormat(DateTimeField::FORMAT_MEDIUM),
            DateTimeField::new('endDate'),
            TextField::new('token')->hideOnForm()->hideOnIndex(),
            AddressField::new('address'),
            AdminField::new('admin')->hideWhenUpdating(),
        ];
    }
}
