<?php

namespace App\Controller\Admin;

use App\Entity\Competition;
use App\Entity\Judge;
use App\Entity\ObstacleConfiguration;
use App\Entity\ObstacleNote;
use App\Entity\Rider;
use App\Entity\TrialType;
use Doctrine\ORM\EntityManagerInterface;
use EasyCorp\Bundle\EasyAdminBundle\Config\Dashboard;
use EasyCorp\Bundle\EasyAdminBundle\Config\MenuItem;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DashboardController extends AbstractDashboardController
{

    public function __construct(private EntityManagerInterface $em)
    {
    }

    #[Route('/admin')]
    public function index(): Response
    {
        //dump($this->getUser());
        $notes = $this->em->getRepository(ObstacleNote::class)
            ->createQueryBuilder("obstacle_note")
            ->addSelect("rider")
            ->leftJoin("obstacle_note.rider", "rider")
            ->leftJoin("rider.competition", "competition")
            ->leftJoin("competition.admin", "admin")
            ->andWhere("admin.id = :admin")
            ->setParameter("admin", $this->getUser()->getId())
            ->getQuery()
            ->getResult();

        //dd($notes);
        $riderNotes = [];

        /* @var $note ObstacleNote */
        foreach ($notes as $note) {
            if (!array_key_exists($note->getRider()->getId()->toRfc4122(), $riderNotes)) {
                $riderNotes[$note->getRider()->getId()->toRfc4122()] = [
                    'rider' => $note->getRider(),
                    'total' => 0,
                    'list' => []
                ];
            }

            $riderNotes[$note->getRider()->getId()->toRfc4122()]['total'] += $note->getTotal();
            $riderNotes[$note->getRider()->getId()->toRfc4122()]['list'][] = $note;
        }

        usort($riderNotes, static function ($a, $b) {
            return $b['total'] <=> $a['total'];
        });

        return $this->render('admin/Dashboard.html.twig', ["riderNotes" => $riderNotes]);
    }

    public function configureDashboard(): Dashboard
    {
        return Dashboard::new()
            ->setTitle('EQUITREC')
            ->renderContentMaximized();
    }

    public function configureMenuItems(): iterable
    {
        yield MenuItem::linkToDashboard('Récapitulatif', 'fa fa-home');

        yield MenuItem::section("Configuration")->setPermission('ROLE_SUPER_ADMIN');

        yield MenuItem::linkToCrud("Type d'épreuve", "fas fa-list",
            TrialType::class)->setPermission('ROLE_SUPER_ADMIN');

        yield MenuItem::linkToCrud("Obstacles", "fa fa-archway",
            ObstacleConfiguration::class)->setPermission('ROLE_SUPER_ADMIN');

        yield MenuItem::section("Application");
        yield MenuItem::linkToCrud('Compétition', 'fas fa-list', Competition::class);
        yield MenuItem::linkToCrud('Juges', 'fas fa-users', Judge::class)->setPermission("ROLE_LOCAL_ADMIN");
        yield MenuItem::linkToCrud('Cavalier', 'fas fa-horse', Rider::class)->setPermission("ROLE_LOCAL_ADMIN");

        yield MenuItem::linkToCrud('Notes', 'fas fa-percent', ObstacleNote::class)->setPermission("ROLE_LOCAL_ADMIN");
    }
}
