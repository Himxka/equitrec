<?php

namespace App\Controller\Admin;

use App\Entity\Rider;
use Doctrine\ORM\QueryBuilder;
use EasyCorp\Bundle\EasyAdminBundle\Collection\FieldCollection;
use EasyCorp\Bundle\EasyAdminBundle\Collection\FilterCollection;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Dto\EntityDto;
use EasyCorp\Bundle\EasyAdminBundle\Dto\SearchDto;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IntegerField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Orm\EntityRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

#[IsGranted('ROLE_LOCAL_ADMIN')]
class RiderCrudController extends AbstractCrudController
{
    public function __construct(
        private readonly EntityRepository $entityRepository,
    ) {
    }

    public static function getEntityFqcn(): string
    {
        return Rider::class;
    }

    public function createIndexQueryBuilder(
        SearchDto $searchDto,
        EntityDto $entityDto,
        FieldCollection $fields,
        FilterCollection $filters
    ): QueryBuilder {
        parent::createIndexQueryBuilder($searchDto, $entityDto, $fields, $filters);

        $response = $this->entityRepository->createQueryBuilder($searchDto, $entityDto, $fields, $filters);
        $response->leftJoin("entity.competition", "competition");
        $response->andWhere("competition.admin = :user")->setParameter("user", $this->getUser());

        return $response;
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            TextField::new('id')->hideOnForm(),
            TextField::new('firstName'),
            TextField::new('lastName'),
            TextField::new('licenceNumber'),
            IntegerField::new('bibNumber'),
            AssociationField::new("competition")->setQueryBuilder(
                fn(QueryBuilder $queryBuilder) => $queryBuilder->addOrderBy('entity.id',
                    "desc")->andWhere("entity.admin = :user")->setParameter("user", $this->getUser())
            )->setCrudController(CompetitionCrudController::class),
        ];
    }
}
