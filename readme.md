# Utilisation de l'application

### Etape n°1 (installation)

* ``git clone git@gitlab.com:MaximeBadin/projet33.git``
* ``composer update``
* ``docker-compose build``
* ``docker-compose up -d``

### Etape  n°2 (Configuration)

* ``docker exec -it -w /var/www/projet33 projet33_php_1 php bin/console d:s:c``
* ``docker exec -it -w /var/www/projet33 projet33_php_1 php bin/console app:create:admin <email>``

### Etape n°3 (Utilisation)

* Ouvrir la page [localhost:8080](http://localhost:8080/admin)

# Mise a jour

### Etape n°1 (Mise a jour code)

* ``git pull``
* ``composer update``

### Etape  n°2 (Mise a jour BDD)

* ``docker exec -it -w /var/www/projet33 projet33_php_1 php bin/console d:s:u --force``
